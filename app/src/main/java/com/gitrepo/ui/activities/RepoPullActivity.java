package com.gitrepo.ui.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gitrepo.R;
import com.gitrepo.api.HttpClient;
import com.gitrepo.model.RepoPull;
import com.gitrepo.ui.adapter.RepoPullAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RepoPullActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    public TextView tvOpened;
    public TextView tvClosed;
    List<RepoPull> repoPulls = new ArrayList<RepoPull>();

    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.repo_pull);

        tvOpened = findViewById(R.id.tv_opened);
        tvClosed = findViewById(R.id.tv_closed);

        Intent intent = getIntent();
        String login = intent.getStringExtra("login");
        String name = intent.getStringExtra("name");

        progressBar = findViewById(R.id.progressBar);

        recyclerView = findViewById(R.id.rv_repo_pull);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);

        HttpClient httpClient = new HttpClient();
        httpClient.getRepoPull(new Callback<List<RepoPull>>() {

            @Override
            public void onResponse(Call<List<RepoPull>> call, Response<List<RepoPull>> response) {
                recyclerView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);

                if (response.isSuccessful()) ;
                recyclerView.setAdapter(new RepoPullAdapter(response.body(), RepoPullActivity.this));

                int isClosed = 0;
                int isOpened = 0;
                for (RepoPull repoPull : response.body()) {
                    if (repoPull.state.equals("closed")) {
                        isClosed++;
                    } else {
                        isOpened++;
                    }
                }
                tvClosed.setText(String.valueOf(isClosed+" closed"));
                tvOpened.setText(String.valueOf(isOpened+" opened"));
            }

            @Override
            public void onFailure(Call<List<RepoPull>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                new AlertDialog.Builder(RepoPullActivity.this).setMessage("Servidor indisponível no momento, por favor tente mais tarde").setPositiveButton("Tentar Novamente", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        progressBar.setVisibility(View.VISIBLE);
                    }
                }).show();
            }
        }, login, name);
    }
}

