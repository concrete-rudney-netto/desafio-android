package com.gitrepo.api;

import com.gitrepo.model.Application;
import com.gitrepo.model.RepoPull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by izabela on 1/7/18.
 */

public class HttpClient {

    RepoService api;

    public HttpClient() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api = retrofit.create(RepoService.class);
    }

    public void getRepo(Callback<com.gitrepo.model.Application> callback, int page) {

        Call<com.gitrepo.model.Application> call = api.getRepo(page);
        call.enqueue(callback);

    }

    public void getRepoPull(Callback<List<RepoPull>> callback, String login, String name) {

        Call<List<RepoPull>> call = api.getRepoPull(login, name);
        call.enqueue(callback);

    }
}

