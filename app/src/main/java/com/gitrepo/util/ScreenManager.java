package com.gitrepo.util;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.gitrepo.ui.activities.RepoPullActivity;
import com.gitrepo.ui.adapter.RepoAdapter;
import com.gitrepo.ui.adapter.RepoPullAdapter;

/**
 * Created by izabela on 12/4/17.
 */

public class ScreenManager {

    public static void goToRepoDescription(Context context, String login, String name) {
        Intent intent = new Intent(context, RepoPullActivity.class);
        intent.putExtra("login", login);
        intent.putExtra("name", name);
        context.startActivity(intent);
    }

    public static void goToRepoPull(Context context, String html_url) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(html_url));
        browserIntent.putExtra("html_url", html_url);
        try {
            context.startActivity(browserIntent);
        } catch (ActivityNotFoundException ex) {
            context.startActivity(browserIntent);
        }
    }
}

