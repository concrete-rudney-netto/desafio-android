package com.gitrepo.model;

public class Base
{
    public String label;
    public String ref;
    public String sha;
    public User user;
    public Repo repo;
}
